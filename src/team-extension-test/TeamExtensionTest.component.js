import React from 'react';
import Image from './services/image.service';
import Lightbox from 'lightbox-react';
import 'lightbox-react/style.css';
import logo from "../logo.svg";

class TeamExtensionTest extends React.Component {
    imageService;
    images = [];

    constructor( props ) {
        super(props);

        this.imageService = new Image();

        this.state = {
            photoIndex: 0,
            isOpen: false
        };

        this.getImages();
    }

    getImages() {
        this.imageService.getImages().then((res) => {

            res.data.items.forEach((item) => {
                try {
                    const imgUrl = item.pagemap.cse_thumbnail[0].src;

                    if (imgUrl) {
                        this.images.push(imgUrl);
                    }

                } catch (e) {
                    console.log(e);
                }

            });
        }).catch((err) => {
            console.log(err);
        });
    }

    render() {
        const { photoIndex, isOpen } = this.state;

        return (
            <div className="App">


                <button type="button" onClick={() => this.setState({ isOpen: true })}>Open Lightbox</button>

                {isOpen && (
                    <Lightbox
                        mainSrc={this.images[photoIndex]}
                        nextSrc={this.images[(photoIndex + 1) % this.images.length]}
                        prevSrc={this.images[(photoIndex + this.images.length - 1) % this.images.length]}
                        onCloseRequest={() => this.setState({ isOpen: false })}
                        onMovePrevRequest={() =>
                            this.setState({
                                photoIndex: (photoIndex + this.images.length - 1) % this.images.length,
                            })
                        }
                        onMoveNextRequest={() =>
                            this.setState({
                                photoIndex: (photoIndex + 1) % this.images.length,
                            })
                        }
                    />
                )}
            </div>
        )
    }
}

export default TeamExtensionTest;